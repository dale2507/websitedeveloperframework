# Static Website Developer Framework #

This repoistory contains a template framework to quickly start creating a static website.

## Setup ##

To start using the framework, use the following instructions:

1. Fork the repository.
2. Modify the contents of `package.json` to reflect your website.
3. Replace this README with instructions for your developers.
4. Follow the instructions at [docs/FrameworkInstructions.md](docs/FrameworkInstructions.md) to start your website.
