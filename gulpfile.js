// Import the required modules.
var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

// Define the paths used for the build.
var paths = {
    html: {
        src: 'src/**/*.html',
        dest: 'dist/'
    },
    images: {
        src: 'src/images/**.*',
        dest: 'dist/images'
    },
    styles: {
        src: 'src/style/**/*.scss',
        dest: 'dist/style/'
    },
    scripts: {
        src: 'src/scripts/**/*.js',
        dest: 'dist/scripts'
    }
}

// Create clean tasks.
function clean() {
    return del('dist/');
}
gulp.task('clean', clean);

// Create build tasks.
function buildHtml() {
    return gulp.src(paths.html.src)
        .pipe(gulp.dest(paths.html.dest));
}
gulp.task('build:html', buildHtml);

function buildImages() {
    return gulp.src(paths.images.src)
        .pipe(gulp.dest(paths.images.dest));
}
gulp.task('build:images', buildImages);

function buildStyles() {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps/'))
        .pipe(gulp.dest(paths.styles.dest));
}
gulp.task('build:styles', buildStyles);

function buildScripts() {
    return gulp.src(paths.scripts.src)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./maps/'))
        .pipe(gulp.dest(paths.scripts.dest));
}
gulp.task('build:scripts', buildScripts);

gulp.task('build', gulp.parallel('build:html', 'build:images', 'build:styles', 'build:scripts'));

// Create rebuild tasks.
gulp.task('rebuild', gulp.series('clean', 'build'));

// Create the watcher.
gulp.task('watch', function() {
    gulp.watch(paths.html.src, buildHtml);
    gulp.watch(paths.images.src, buildImages);
    gulp.watch(paths.styles.src, buildStyles);
    gulp.watch(paths.scripts.src, buildScripts);
});