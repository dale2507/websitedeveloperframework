# Framework Instructions #

The website contained within this repository is built on the Website Developer Framework available at [https://bitbucket.org/dale2507/websitedeveloperframework](https://bitbucket.org/dale2507/websitedeveloperframework). This page contains instructions on how to use this framework.

## Setup ##

In order to use this framework, this guide assumes the use of Visual Studio Code.

You will need to have installed:

* [Visual Studio Code](https://code.visualstudio.com/)
* [Node.js](https://nodejs.org/en/) (8.9.4 or higher)

Once you have the above installed:

1. Clone the repository to your computer
2. Open the folder in Visual Studio Code
3. Press `F1` and select "Tasks: Run Task" (or select "Tasks" > "Run Task..." from the menu)
4. Select the "First run" task

This will install the prerequisites to run the framework.

## Development ##

There are a number of entry tasks for the build. These are:

* **Clean:** Removes all built files to ensure a clean build environment.
* **Build:** Compiles, minifies and transfers the files required for the website to the `dist` directory.
* **Rebuild:** Executes, `Clean` followed by `Build`.

Once `Build` or `Rebuild` has finished executing, the files within the `dist` folder will contain all of the files needed to publish the website.

Additionally, a "watcher" is available which will automatically build the website when files change. This can be started so that the build does not have to be executed manually. **Note that this is only to be used for development as it does not clean the build environment so can leave unused files in the dist folder.**

To execute build tasks, press `F1` and select "Tasks: Run Task" (or select "Tasks" > "Run Task..." from the menu). A list of tasks which can be executed will be displayed.

## Folders ##

In order for the build to execute correctly, the file strucutre must be correct.

All source files for the website should be stored in the `src` folder. This is then split as follows:

* **HTML Files:** HTML files should be placed in the root of the `src` folder. They can also be placed in subfolders which will be preserved when the site is built. All HTML files should have the `.html` file extension.
* **Style Files:** Style files should be placed in `src/style/`. They can also be placed in subfolders which will be preserved when the site is built. All style files should use the [SCSS](https://sass-lang.com/guide) syntax (Note that this is different to SASS), and should use the `.scss` file extension. If you are not familiar with scss, you can write css with the `.scss` file extension as it is backwards compatible.
* **Script Files:** Script files should be placed in `src/scripts/`. They can also be placed in subfolders which will be preserved when the site is built. All script files should use the `.js` file extension.
* **Image Files:** Image files should be placed in `src/images/`. They can also be placed in subfolders which will be preserved when the site is built. All files within the images folder will be transferred but it is recommended to use PNG, JPEG or WEBP formats for future compatibility.